/*
 * Copyright (c) 2013, TRIOLOGY GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * https://www.scm-manager.com
 *
 */

Ext.ns('Triology');
Ext.ns('Triology.groupmanager');

Triology.groupmanager.I18n = {
  groupManagerText : 'Group Manager',
  emptyText : 'Select a group.',
  removeTitleText : 'Remove Group',
  removeMsgText : 'Remove Group "{0}"?',
  errorTitleText : 'Error',
  errorMsgText : 'Group deletion failed',
  colNameText : 'Name',
  colDescriptionText : 'Description',
  colMembersText : 'Members',
  colGroupManagerText : 'Group-Manager',
  colCreationDateText : 'Creation date',
  colTypeText : 'Type',
  emptygroupManagerStoreText : 'There are no groups configured you can manage.',
  groupFormTitleText : 'Group Form',
  settingsText : 'Settings',
  updateErrorMsgText : 'Group update failed',
  createErrorMsgText : 'Group creation failed',
  groupManagerHelpText : 'Usernames of the Group-Managers.',
  groupManagerFormTitleText : 'Group Manager Form',
  sectionSecurityText : 'Security'

};

// German translation

if ('de' == i18n.country) {
  Triology.groupmanager.I18n = {
    groupManagerText : 'Gruppen-Manager',
    emptyText : 'Wählen Sie eine Gruppe aus.',
    removeTitleText : 'Gruppe entfernen',
    removeMsgText : 'Gruppe "{0}" entfernen?',
    errorTitleText : 'Fehler',
    errorMsgText : 'Das Löschen der Gruppe ist fehlgeschlagen!',
    colNameText : 'Name',
    colDescriptionText : 'Beschreibung',
    colMembersText : 'Mitglieder',
    colGroupManagerText : 'Gruppen-Manager',
    colCreationDateText : 'Erstellungsdatum',
    colTypeText : 'Typ',
    emptygroupManagerStoreText : 'Es sind keine Gruppen konfiguriert, die Sie verwalten können.',
    groupFormTitleText : 'Gruppen Formular',
    settingsText : 'Einstellungen',
    updateErrorMsgText : 'Update der Gruppen-Manager fehlgeschalgen!',
    createErrorMsgText : 'Erstellung des Gruppen-Managers fehlgeschlagen!',
    groupManagerHelpText : 'Die Benutzernamen der Gruppen-Manager.',
    groupManagerFormTitleText : 'Gruppen-Manager Formular',
    sectionSecurityText : 'Sicherheit'
  };
}

loginCallbacks.push(function() {
  var navPanel = Ext.getCmp('navigationPanel');
  var securitySection = navPanel.items.get("securityConfig");

  var link = {
    label : Triology.groupmanager.I18n.groupManagerText,
    fn : Triology.groupmanager.addGroupManagerTabPanel,
    scope : this
  };

  // not XML users have no security section
  if (!securitySection){
    var count = navPanel.count() - 1;
    navPanel.insertSection(count, {
      id: 'securityConfig',
      title: Triology.groupmanager.I18n.sectionSecurityText,
      links: [link]
    });
  } else {
    securitySection.addLink(link);
    securitySection.tpl.overwrite(securitySection.body, {
      links: securitySection.links
    });
  }
});

Triology.groupmanager.addGroupManagerTabPanel = function() {
  main.addTabPanel('groupmanagerPanel', 'groupmanagerPanel', Triology.groupmanager.I18n.groupManagerText);
};

Triology.groupmanager.setEditPanel = function(panels) {
  var editPanel = Ext.getCmp('groupmanagerEditPanel');
  editPanel.removeAll();
  Ext.each(panels, function(panel) {
    editPanel.add(panel);
  });
  editPanel.setActiveTab(0);
  editPanel.doLayout();
};

/**
 * panels
 */
Triology.groupmanager.DefaultPanel = {
  region : 'south',
  title : Triology.groupmanager.I18n.groupManagerFormTitleText,
  xtype : 'panel',
  bodyCssClass : 'x-panel-mc',
  padding : 5,
  html : Triology.groupmanager.I18n.emptyText
};
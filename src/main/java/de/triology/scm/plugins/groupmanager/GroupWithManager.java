/**
 * Copyright (c) 2013, TRIOLOGY GmbH
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * https://www.scm-manager.com
 * 
 */
package de.triology.scm.plugins.groupmanager;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import sonia.scm.group.Group;

import com.google.common.base.Objects;

/**
 * Extends the group class with group managers.
 * 
 * @author Jan Boerner, TRIOLOGY GmbH
 */
public class GroupWithManager extends Group {

  /** Serial version unique identifier. */
  private static final long serialVersionUID = -7919097565099271339L;

  /** managers of this group */
  private List<String> groupmanager;

  // ~--- constructors ---------------------------------------------------------

  /**
   * Constructs {@link GroupWithManager} object. This constructor is required by JAXB.
   */
  public GroupWithManager() {
    super();
  }

  /**
   * Constructs {@link GroupWithManager} object.
   * 
   * @param type of the group
   * @param name of the group
   * @param manager of the group
   */
  public GroupWithManager(final String type, final String name, final List<String> members, final List<String> manager) {
    super(type, name, members);
    groupmanager = manager;
  }

  /**
   * Constructs a GroupWithManager object using a Group object.
   * @param group Base for the new object.
   */
  public GroupWithManager(final Group group) {
    this(group.getType(), group.getName(), group.getMembers(), new LinkedList<String>());
    setDescription(group.getDescription());
    setCreationDate(group.getCreationDate());
    setProperties(group.getProperties());
    setLastModified(group.getLastModified());

  }

  /**
   * Add a new manager to the group.
   * 
   * @param manager - The name of new group manager
   * @return true if the operation was successful
   */
  public boolean addManager(final String manager) {
    return getGroupmanager().add(manager);
  }

  /**
   * Add a list of managers to the group.
   * @param managers The collection of group managers
   * @return true, f the operation was successful.
   */
  public boolean addManagers(final Collection<String> managers) {
    if (null != managers) {
      return getGroupmanager().addAll(managers);
    }
    return false;
  }

  /**
   * Remove all group managers of the group.
   */
  public void clearManager() {
    groupmanager.clear();
  }

  /**
   * Copies all properties of this group to the given one.
   * 
   * @param group to copies all properties of this one
   */
  public void copyProperties(final GroupWithManager group) {
    super.copyProperties(group);
    group.setGroupmanager(getGroupmanager());
  }

  /**
   * Returns true if this {@link GroupWithManager} is the same as the obj argument.
   * 
   * @param obj - the reference object with which to compare
   * @return true if this {@link GroupWithManager} is the same as the obj argument
   */
  @Override
  public boolean equals(final Object obj) {

    if (super.equals(obj)) {
      final GroupWithManager other = (GroupWithManager) obj;
      return Objects.equal(groupmanager, other.groupmanager);
    }

    return false;
  }

  /**
   * Returns a hash code value for this {@link Group}.
   * 
   * @return a hash code value for this {@link Group}
   */
  @Override
  public int hashCode() {
    return Objects.hashCode(super.hashCode(), getGroupmanager());
  }

  /**
   * Returns true if the user is a group manager of this group.
   * 
   * @param user - The name of the user
   * @return true if the member is a member of this group
   */
  public boolean isGroupManager(final String user) {
    return (groupmanager != null) && groupmanager.contains(user);
  }

  /**
   * Remove the given manager from this group.
   * 
   * @param manager to remove from this group
   * @return true if the operation was successful
   */
  public boolean removeGroupManager(final String manager) {
    return groupmanager.remove(manager);
  }

  /**
   * Returns a {@link String} that represents this group.
   * 
   * @return a {@link String} that represents this group
   */
  @Override
  public String toString() {
    // J-
    return super.toString().concat(Objects.toStringHelper(this).add("groupmanager", groupmanager).toString());
    // J+
  }

  /**
   * @return the groupmanagers
   */
  public List<String> getGroupmanager() {
    return groupmanager;
  }

  /**
   * @param groupmanagers the groupmanagers to set
   */
  public void setGroupmanager(final List<String> groupmanagers) {
    groupmanager = groupmanagers;
  }

}
